package cz.korunka.myapplication.rest.main

import cz.korunka.myapplication.Constants
import cz.korunka.myapplication.api.calls.Units
import cz.korunka.myapplication.api.calls.main.GetCityWeatherTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MainService {


    @GET("data/2.5/weather")
    suspend fun getWeatherById(@Query("id") id:Long = Constants.ID_PRAGUE,@Query("appid") appid:String = Constants.API_KEY, @Query("units") units: String = Units.METRIC.name): Response<GetCityWeatherTO>

    @GET("data/2.5/weather")
    suspend fun getWeatherByLongLan(@Query("lon") lon:Double ,@Query("lat") lat:Double ,@Query("appid") appid:String = Constants.API_KEY, @Query("units") units: String = Units.METRIC.name): Response<GetCityWeatherTO>

}