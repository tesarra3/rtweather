package cz.korunka.myapplication.rest.main

import cz.korunka.myapplication.api.calls.main.GetCityWeatherTO
import retrofit2.Response


interface MainHelper {

    suspend fun getWeatherInPrague(): Response<GetCityWeatherTO>

    suspend fun getWeatherById(id: Long): Response<GetCityWeatherTO>

    suspend fun getWeatherByLongLan(long: Double, lan:Double): Response<GetCityWeatherTO>

}