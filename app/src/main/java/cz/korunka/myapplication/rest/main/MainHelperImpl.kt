package cz.korunka.myapplication.rest.main

import cz.korunka.myapplication.api.calls.main.GetCityWeatherTO
import retrofit2.Response
import javax.inject.Inject

class MainHelperImpl @Inject constructor(
    private val mainService: MainService
) : MainHelper {
    override suspend fun getWeatherInPrague(): Response<GetCityWeatherTO> =
        mainService.getWeatherById()

    override suspend fun getWeatherById(id: Long): Response<GetCityWeatherTO> =
        mainService.getWeatherById(id = id)

    override suspend fun getWeatherByLongLan(
        long: Double,
        lan: Double
    ): Response<GetCityWeatherTO> = mainService.getWeatherByLongLan(long, lan)

}