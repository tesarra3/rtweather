package cz.korunka.myapplication.hilt

import android.content.Context
import androidx.room.Room
import cz.korunka.myapplication.database.AppDatabase
import cz.korunka.myapplication.database.dao.CityWeatherDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Provides
    fun provideCityWeatherDao(appDatabase: AppDatabase): CityWeatherDao {
        return appDatabase.cityWeatherDao()
    }


    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "Weather"
        ).build()
    }
}