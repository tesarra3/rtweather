package cz.korunka.myapplication.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import cz.korunka.myapplication.base.BaseFragment
import cz.korunka.myapplication.database.entity.CityWeather
import cz.korunka.myapplication.databinding.FragmentSecondBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class SecondFragment : BaseFragment<MainViewModel, FragmentSecondBinding>(MainViewModel::class.java) {

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
    ): FragmentSecondBinding {
        return FragmentSecondBinding.inflate(inflater, container, false)
    }


    /** vlakno pro api */
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: SecondFragmentArgs by navArgs()
        binding?.apply {
            observe(viewModel.cities){
                if(pager.adapter == null) {
                    pager.adapter = MyTicketPagerAdapter(this@SecondFragment, it)

                    TabLayoutMediator(tybLayout, pager) { tab, position ->
                        tab.text = it[position].cityName
                    }.attach()
                    tybLayout.tabMode = TabLayout.MODE_SCROLLABLE

                    scope.launch(Dispatchers.Default) {
                        it.forEachIndexed { index, cityWeather ->
                            if(cityWeather.cityId ==  args.cityCodeArg){
                                withContext(Dispatchers.Main) {
                                    pager.currentItem = index
                                }
                                return@forEachIndexed
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Pager for cities
     */
    class MyTicketPagerAdapter(fragmentMain: Fragment, val data: List<CityWeather>) : FragmentStateAdapter(fragmentMain) {

        override fun getItemCount(): Int = data.size

        override fun createFragment(position: Int): Fragment {

            return ThirdFragment(data[position].cityId)
        }
    }
}






