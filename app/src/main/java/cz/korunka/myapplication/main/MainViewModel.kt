package cz.korunka.myapplication.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.core.app.ActivityCompat
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import cz.korunka.myapplication.api.base.Resource
import cz.korunka.myapplication.api.calls.main.GetCityWeatherTO
import cz.korunka.myapplication.base.BaseViewModel
import cz.korunka.myapplication.database.entity.CityWeather
import kotlinx.coroutines.launch


class MainViewModel  @ViewModelInject constructor(
    private val repository: MainRepository,
    val dataManager: MainDatamanager
) : BaseViewModel() {

    var idOfCity = 0L

    val cityWeather : LiveData<CityWeather>
        get() = repository.getCity(idOfCity)

    val weather : LiveData<Resource<Any>>
        get() = dataManager._weather

    val cities : LiveData<List<CityWeather>>
        get() = repository.getCities()


    fun getWeather()  = viewModelScope.launch {
        dataManager._weather.postValue(Resource.loading(null))
        repository.getWeatherInPrague().let {
            if (it.isSuccessful){
                dataManager._weather.postValue(Resource.success(it.body()))
            }else{
                dataManager._weather.postValue(
                    Resource.error(
                        it.message(),
                        it.errorBody(),
                        it.code()
                    )
                )
            }
        }
    }

    fun getWeatherById(id: Long)  = viewModelScope.launch {
        dataManager._weather.postValue(Resource.loading(null))
        repository.getWeatherById(id).let {
            if (it.isSuccessful){
                dataManager._weather.postValue(Resource.success(it.body()))
            }else{
                dataManager._weather.postValue(
                    Resource.error(
                        it.message(),
                        it.errorBody(),
                        it.code()
                    )
                )
            }
        }
    }


    fun getWeatherByLongLan(longitude: Double, latitude: Double) = viewModelScope.launch {
        dataManager._weather.postValue(Resource.loading(null))
        repository.getWeatherByLongLan(longitude,latitude).let {
            if (it.isSuccessful){
                dataManager._weather.postValue(Resource.success(it.body()))
            }else{
                dataManager._weather.postValue(
                    Resource.error(
                        it.message(),
                        it.errorBody(),
                        it.code()
                    )
                )
            }
        }
    }

    fun saveNewData(data: GetCityWeatherTO) {
        viewModelScope.launch{
            repository.insertCity(data)
        }
    }

}