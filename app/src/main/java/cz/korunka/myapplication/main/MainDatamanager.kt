package cz.korunka.myapplication.main

import androidx.lifecycle.MutableLiveData
import cz.korunka.myapplication.api.base.Resource
import javax.inject.Inject

class MainDatamanager @Inject constructor()  {

    val _weather = MutableLiveData<Resource<Any>>()
}