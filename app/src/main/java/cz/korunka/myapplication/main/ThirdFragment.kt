package cz.korunka.myapplication.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.korunka.myapplication.R
import cz.korunka.myapplication.api.base.Status
import cz.korunka.myapplication.api.calls.main.GetCityWeatherTO
import cz.korunka.myapplication.base.BaseFragment
import cz.korunka.myapplication.databinding.FragmentThirtBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ThirdFragment(val id:Long) : BaseFragment<MainViewModel, FragmentThirtBinding>(MainViewModel::class.java) {

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
    ): FragmentThirtBinding {
        return FragmentThirtBinding.inflate(inflater, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.idOfCity = id
        binding?.apply{
            swiperefresh.setOnRefreshListener {
                viewModel.getWeatherById(id)
            }

            observe(viewModel.cityWeather){
                    tempriture.text = requireContext().getString(R.string.tempriture_in_celsius,it.temp.toString())
                    humidity.text = requireContext().getString(R.string.humidity,it.humidity.toString())
                    wind.text = requireContext().getString(R.string.wind,it.windSpeed.toString())
                    presure.text = requireContext().getString(R.string.presure,it.pressure.toString())
            }

            observe(viewModel.weather){
                swiperefresh.isRefreshing = false
                when(it.status){
                    Status.SUCCESS -> {
                        if(it.data is GetCityWeatherTO){
                            viewModel.saveNewData(it.data)
                        }
                    }

                    else -> {
                        handleGeneralErrors(it.data,it.message,it.code)
                    }
                }
            }
        }
    }
}