package cz.korunka.myapplication.main

import cz.korunka.myapplication.api.calls.main.GetCityWeatherTO
import cz.korunka.myapplication.database.dao.CityWeatherDao
import cz.korunka.myapplication.database.entity.CityWeather
import cz.korunka.myapplication.rest.main.MainHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainRepository @Inject constructor(

    private val apiHelper: MainHelper,
    private val cityWeatherDao: CityWeatherDao
)  {

    fun getCities() = cityWeatherDao.getAll()

    fun getCity(id:Long) = cityWeatherDao.findByCityIdLive(id)

    suspend fun getWeatherInPrague() = apiHelper.getWeatherInPrague()

    suspend fun getWeatherById(id:Long) = apiHelper.getWeatherById(id)

    suspend fun getWeatherByLongLan(long: Double, lan:Double) = apiHelper.getWeatherByLongLan(long, lan)

    suspend fun insertCity(data: GetCityWeatherTO) {
        withContext(Dispatchers.Default){
            cityWeatherDao.findByCityId(data.id?:0L)?.let{ cw ->
                cw.temp = data.main?.temp?:0.toDouble()
                cw.pressure = data.main?.pressure?:0.toDouble()
                cw.humidity = data.main?.humidity?:0.toDouble()
                cw.windSpeed = data.wind?.speed?:0.toDouble()
                cityWeatherDao.updateAll(cw)

            }?:run{
                val cw = CityWeather(
                    temp = data.main?.temp?:0.toDouble(),
                    pressure = data.main?.pressure?:0.toDouble(),
                    humidity = data.main?.humidity?:0.toDouble(),
                    windSpeed = data.wind?.speed?:0.toDouble(),
                    cityName = data.name?:"",
                    cityId = data.id?:0L
                )
                cityWeatherDao.insertAll(cw)
            }
        }
    }
}