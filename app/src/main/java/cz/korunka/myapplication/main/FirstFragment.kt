package cz.korunka.myapplication.main

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cz.korunka.myapplication.api.base.Status
import cz.korunka.myapplication.api.calls.main.GetCityWeatherTO
import cz.korunka.myapplication.base.BaseFragment
import cz.korunka.myapplication.databinding.FragmentFirstBinding
import dagger.hilt.android.AndroidEntryPoint


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class FirstFragment : BaseFragment<MainViewModel, FragmentFirstBinding>(MainViewModel::class.java) {


    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
    ): FragmentFirstBinding {
        return FragmentFirstBinding.inflate(inflater, container, false)
    }

    companion object{
        val REQUEST_CODE_LOCATION= 123
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(viewModel.weather){
            when(it.status){
                Status.SUCCESS -> {
                    if (it.data is GetCityWeatherTO) {
                        viewModel.saveNewData(it.data)
                        findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToSecondFragment(it.data.id?:0L))

                    }

                }
                Status.LOADING -> {
                    binding?.progressBar?.visibility = View.VISIBLE
                }
                else -> {
                    handleGeneralErrors(it.data, it.message, it.code)
                }
            }
        }
        getLocation()
    }


    fun getLocation(){
        val mLocationManager: LocationManager =   requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            checkPermission(REQUEST_CODE_LOCATION,arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION))
            return
        }

        mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)?.let{ netLocation ->
            viewModel.getWeatherByLongLan(netLocation.longitude,netLocation.latitude)
        }?:run{
            viewModel.getWeather()
        }
    }

    override fun onCheckPermissionsResult(
        nRequestCode: Int,
        bGrantedAll: Boolean,
        aPermissions: Array<String>?,
        aGrantResults: IntArray
    ) {
        when(nRequestCode){
            REQUEST_CODE_LOCATION -> {
                if(bGrantedAll) {
                    getLocation()
                } else {
                    viewModel.getWeather()
                }
            }
        }
    }
}