package cz.korunka.myapplication.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CityWeather(
    @PrimaryKey(autoGenerate = true) val id: Int = 0 ,
    @ColumnInfo(name = "temp") var temp: Double,
    @ColumnInfo(name = "pressure") var pressure: Double,
    @ColumnInfo(name = "humidity")
    var humidity: Double,
    @ColumnInfo(name = "windSpeed")
    var windSpeed: Double,
    @ColumnInfo(name = "cityName")
    val cityName: String,
    @ColumnInfo(name = "cityId")
    val cityId: Long,
)
