package cz.korunka.myapplication.database

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.korunka.myapplication.database.dao.CityWeatherDao
import cz.korunka.myapplication.database.entity.CityWeather

@Database(entities = [CityWeather::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cityWeatherDao(): CityWeatherDao
}