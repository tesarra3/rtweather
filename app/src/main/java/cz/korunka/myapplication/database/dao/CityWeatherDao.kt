package cz.korunka.myapplication.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import cz.korunka.myapplication.database.entity.CityWeather

@Dao
interface CityWeatherDao {
    @Query("SELECT * FROM cityWeather")
    fun getAll(): LiveData<List<CityWeather>>

    @Query("SELECT * FROM cityWeather WHERE id IN (:ids)")
    fun loadAllByIds(ids: IntArray): List<CityWeather>

    @Query("SELECT * FROM cityWeather WHERE cityName LIKE (:name) LIMIT 1")
    fun findByName(name: String): CityWeather

    @Query("SELECT * FROM cityWeather WHERE cityId LIKE (:id) LIMIT 1")
    fun findByCityId(id: Long): CityWeather?

    @Query("SELECT * FROM cityWeather WHERE cityId LIKE (:id) LIMIT 1")
    fun findByCityIdLive(id: Long): LiveData<CityWeather>

    @Insert
    fun insertAll(vararg cityWeather: CityWeather)

    @Update
    fun updateAll(vararg cityWeather: CityWeather)

    @Delete
    fun delete(cityWeather: CityWeather)
}