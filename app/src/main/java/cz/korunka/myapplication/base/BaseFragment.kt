package cz.korunka.myapplication.base

import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.Snackbar
import cz.korunka.myapplication.R

abstract class BaseFragment<T : BaseViewModel, V : ViewBinding>(
private val viewModelClass: Class<T>,
@LayoutRes private val layoutId: Int
) : Fragment() {

    constructor(viewModelClass: Class<T>) : this(viewModelClass, -1)

    protected val viewModel: T by lazy { ViewModelProvider(this).get(viewModelClass) }


    private var _binding: V? = null

    /**
     * dont call in classes with layout itin
     */
    protected val binding get() = _binding

    /**
     *  if waiting for permition
     */
    private var checkPermissionPending: Boolean = false

    /**
     * sync for requesting permition
     */
    private val checkPermissionPendingLock = Any()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycle.addObserver(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflateBinding(inflater, container)
        val rootView = if (_binding == null && layoutId != -1) {
            inflater.inflate(layoutId, container, false)
        } else {
            _binding?.let{
                it.root
            }
        }


        return rootView
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * LayoutBinding class T.inflate(inflater, container, Boolean)
     */
    abstract fun inflateBinding(inflater: LayoutInflater, container: ViewGroup?): V?


    /**
     * Base observer for live data
     */
    protected fun <T> observe(liveData: LiveData<T>?, observeFun: (T) -> Unit) {
        liveData?.observe(viewLifecycleOwner) { it ->
            it?.let {
                observeFun.invoke(it)
            }
        }
    }

    /**
     * Check permition with dialog popups
     *
     * @param requestId id request
     * @param permissions list of permitions
     */
    protected fun checkPermission(requestId: Int, permissions: Array<String>) {
        if (context == null) {
            val aGrantResults = IntArray(permissions.size)
            for (i in aGrantResults.indices) {
                aGrantResults[i] = PackageManager.PERMISSION_DENIED
            }
            onRequestPermissionsResult(requestId, permissions, aGrantResults)
            return
        }

        synchronized(checkPermissionPendingLock) {
            if (!checkPermissionPending) {
                checkPermissionPending = true

                var nPermission = PackageManager.PERMISSION_GRANTED

                val oForPermit = ArrayList<String>()

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    for (strPermissionName in permissions) {
                        if (ContextCompat.checkSelfPermission(
                                requireContext(),
                                strPermissionName
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            oForPermit.add(strPermissionName)
                            nPermission = PackageManager.PERMISSION_DENIED
                        }
                    }
                }

                if (nPermission == PackageManager.PERMISSION_GRANTED) {
                    val aGrantResults = IntArray(permissions.size)
                    for (i in aGrantResults.indices) {
                        aGrantResults[i] = PackageManager.PERMISSION_GRANTED
                    }
                    onRequestPermissionsResult(requestId, permissions, aGrantResults)
                } else {
                    requestPermissions(oForPermit.toTypedArray(), requestId)
                }
            }
        }
    }

    /**
         * permition check
     * @param permissions list of permitions
     * @return true if they are GRANTED
     */
    fun checkPermission(permissions: Array<String>): Boolean {
        val context = context ?: return false

        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }


    /**
     * If i neet check only one from list as Garanted
     * @param permissions list of  permissions
     * @return true if is atleast one GRANTED
     */
    private fun checkOneOfPermission(permissions: Array<String>): Boolean {
        val context = context ?: return false

        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                return true
            }
        }
        return false
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        var grantedAll = true

        synchronized(checkPermissionPendingLock) {
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    grantedAll = false
                    break
                }
            }

            checkPermissionPending = false
        }

        onCheckPermissionsResult(requestCode, grantedAll, permissions, grantResults)
    }


    /**
     * retruning results from permitions check
     * @param nRequestCode code of reqeust
     * @param bGrantedAll all permitions garanted
     * @param aPermissions list of tested permissions
     * @param aGrantResults results of tested permissions
     */
    open fun onCheckPermissionsResult(
        nRequestCode: Int,
        bGrantedAll: Boolean,
        aPermissions: Array<String>?,
        aGrantResults: IntArray
    ) {
    }


    /** handle of general exceptions
     * @param data data of error
     * @param message error message
     * @param code code of error
     */
    fun handleGeneralErrors(data: Any?, message: String?, code: Int?) {

        when(code){
            500 -> {
                message?.let {
                    view?.let { it1 -> Snackbar.make(it1, it, Snackbar.LENGTH_SHORT).show() }
                }?:run{
                    view?.let { it1 -> Snackbar.make(it1, requireContext().getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT).show() }
                }
            }

//            .
//            .
//            .
//            .

            else -> {
                message?.let {
                    view?.let { it1 -> Snackbar.make(it1, it, Snackbar.LENGTH_SHORT).show() }
                }?:run{
                    view?.let { it1 -> Snackbar.make(it1, requireContext().getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT).show() }
                }
            }

        }


    }

}