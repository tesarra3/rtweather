package cz.korunka.myapplication.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<V : ViewBinding> : AppCompatActivity() {


    private var _binding: V? = null

    protected val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        _binding = inflateBinding(layoutInflater)
        super.setContentView(binding.root)
    }


    /**
     * LayoutBinding trida T.inflate(inflater, container, Boolean)
     */
    abstract fun inflateBinding(inflater: LayoutInflater) : V?
}