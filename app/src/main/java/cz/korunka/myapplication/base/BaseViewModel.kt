package cz.korunka.myapplication.base

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel

/**
 * base class for all viewmodels
 */
abstract class BaseViewModel : ViewModel(), LifecycleObserver {


}