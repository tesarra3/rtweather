package cz.korunka.myapplication.api.base

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}