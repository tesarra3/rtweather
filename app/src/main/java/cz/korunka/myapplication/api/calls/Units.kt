package cz.korunka.myapplication.api.calls

enum class Units {
    METRIC,
    IMPERIAL
}