package cz.korunka.myapplication.api.calls.main

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * ifno about weather in city
 */
data class GetCityWeatherTO(
    /** coordinates */
    var coord: Coord? = null,

    /** type of weather */
    var weather: List<Weather>? = null,


    var base: String? = null,

    /** main ifno like tempriture, humidity ... */
    var main: Main? = null,

    var visibility: Int? = null,

    /** wind iformation */
    var wind: Wind? = null,

    /** cluds detail */
    var clouds: Clouds? = null,


    var dt: Int? = null,

    /** systems info */
    var sys: Sys? = null,


    var id: Long? = null,


    var name: String? = null,


    var cod: Int? = null,

    ) : Serializable

data class Sys(
    var type: Int? = null,

    var id: Int? = null,

    var message: Double? = null,

    var country: String? = null,

    var sunrise: Int? = null,

    var sunset: Int? = null
) : Serializable

data class Clouds(var all: Int? = null) : Serializable


data class Wind(
    var speed: Double? = null,

    var deg: Int? = null
) : Serializable

data class Main(

    var temp: Double? = null,

    var pressure: Double? = null,

    var humidity: Double? = null,

    var tempMin: Double? = null,

    var tempMax: Double? = null,
) : Serializable

/** weather with desctiption and icon */
data class Weather(
    var id: Int? = null,

    /** type of weather */
    var main: String? = null,

    /** description of weather */
    var description: String? = null,

    /** icon of weather */
    var icon: String? = null
) : Serializable


data class Coord(
    /** logtitude */
    var lon: Double? = null,

    /** latitude */
    var lat: Double? = null
) : Serializable
